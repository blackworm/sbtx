# sbtx

#### 介绍
基于标准RBAC风格，Spring Security OAuth认证授权的springboot脚手架系统，适用于web系统、第三方、app以及小程序等，如只是单纯web系统请参考sbt系统。

#### 功能介绍

1. 集成了Spring Security OAuth2，支持全面的权限控制

2. 数据库采用了MySQL 8.0，使用Hikari连接池

3. 使用Bootstrap风格的Swagger接口文档说明，方便开发调试

4. 封装远程调用工具类，轻松调用GET、POST、PUT、DELETE服务

5. 全局统一使用FastJson进行序列化与反序列化

6. 集成了Mybatis Plus 简化开发，支持自动生成代码，开发环境集成p6spy做SQL性能分析

7. 集成了缓存Redis，使用Jedis做连接池，并提供操作Redis的工具类

8. 集成了消息队列RabbitMQ

9. 集成了邮件功能

10. 集成了定时任务

11. 集成了多线程异步调用

12. 预留国际化处理

13. 项目规范了返回前端的统一格式，全局代码采用阿里代码规范

14. 全局异常处理（包括自定义业务异常），保证返回前端的结果结构一致

#### OAuth演示

采用redis存储，示例中约定除了/noAuth/**匹配的资源外其他全部需要认证

访问受保护资源，如下：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153515_f4b37121_5541360.png "image-20191216152219926.png")

访问不需要认证的资源，如下：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153538_7035eae3_5541360.png "image-20191216152914286.png")

    ##### 1、用户名密码模式

①、获取token

![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153552_df8f2fa3_5541360.png "image-20191216152323454.png")

②、获取到token后访问受保护的资源

![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153610_4f76da17_5541360.png "image-20191216152353657.png")

③、携带错误的token

![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153627_e2d0efbd_5541360.png "image-20191216152426711.png")

④、刷新token
![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153648_a152bb9d_5541360.png "image-20191216152751400.png")

##### 2、授权码模式

①、访问 http://localhost:8080/oauth/authorize?response_type=code&client_id=sbtx&redirect_uri=http://www.baidu.com&scope=all 输入用户名密码认证返回/?code=P00t56VW734njMgV94TJyva4l

②、用code码获取token

![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/153713_2c1c03b7_5541360.png "image-20191216152648714.png")

③、访问资源效果、刷新token同以上的用户名密码模式

#### 参与贡献



#### 联系与赞助

联系微信：Hello_S10     联系QQ：184377902   邮箱：wanghaifeng_b69s10@aliyun.com

<img src="https://images.gitee.com/uploads/images/2019/1211/193635_a36a4999_5541360.png" style="zoom: 50%;" />

<img src="https://images.gitee.com/uploads/images/2019/1211/193648_a230b776_5541360.png" style="zoom:25%;" />