package com.haifeng.sbtx.mapper.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_dept")
@ApiModel(value="Dept对象", description="部门表")
public class Dept implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "部门编号")
    @TableField("dept_code")
    private String deptCode;

    @ApiModelProperty(value = "部门名称")
    @TableField("dept_name")
    private String deptName;

    @ApiModelProperty(value = "上级部门")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "部门在当前层级下的顺序")
    @TableField("dept_sort")
    private Integer deptSort;

    @ApiModelProperty(value = "部门层级")
    @TableField("dept_level")
    private Integer deptLevel;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否可用：0-不可用；1-可用")
    private Boolean enabled;

    @ApiModelProperty(value = "创建人id")
    @TableField("create_id")
    private Long createId;

    @ApiModelProperty(value = "创建人名称")
    @TableField("create_name")
    private String createName;

    @ApiModelProperty(value = "创建人ip")
    @TableField("create_ip")
    private String createIp;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_datetime")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新人id")
    @TableField("update_id")
    private Long updateId;

    @ApiModelProperty(value = "更新人名称")
    @TableField("update_name")
    private String updateName;

    @ApiModelProperty(value = "更新人ip")
    @TableField("update_ip")
    private String updateIp;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_datetime")
    private LocalDateTime updateDatetime;


}
