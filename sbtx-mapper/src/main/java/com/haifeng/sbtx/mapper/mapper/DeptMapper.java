package com.haifeng.sbtx.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haifeng.sbtx.mapper.entity.Dept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
