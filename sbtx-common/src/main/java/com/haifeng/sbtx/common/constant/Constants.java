package com.haifeng.sbtx.common.constant;

/**
 * <p>
 *  常量类
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-10
 */
public class Constants {

    public static final String USER_ = "user_";
    public static final String DOU = ",";
    public static final String NOAUTH = "/noAuth/**";
    public static final String BEARER = "bearer ";
    public static final String INVALID_REFRESH_TOKEN = "Invalid refresh token";
    public static final String INVALID_AUTHORIZATION_CODE = "Invalid authorization code";
    public static final String BAD_CREDENTIALS = "Bad credentials";
    public static final String TOKEN_WAS_NOT_RECOGNISED = "Token was not recognised";
    public static final String INVALID_SCOPE = "Invalid scope";
    public static final String UNSUPPORTED_GRANT_TYPE = "Unsupported grant type";
    public static final String MISSING_GRANT_TYPE = "Missing grant type";
    public static final String AN_AUTHORIZATION_CODE_MUST_BE_SUPPLIED = "An authorization code must be supplied";

}
