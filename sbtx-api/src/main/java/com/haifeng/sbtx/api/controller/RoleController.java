package com.haifeng.sbtx.api.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@RestController
@RequestMapping("/role")
public class RoleController {

}

