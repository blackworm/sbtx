package com.haifeng.sbtx.api.mq;

import io.netty.util.CharsetUtil;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  RabbitMQ消费类
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-10
 */
@Component
public class RabbitMQListener {

    @RabbitListener(queues = {"hello.direct"})
    public void receive(Message message){
        System.out.println("receive:"+new String(message.getBody(), CharsetUtil.UTF_8));
    }

    @RabbitListener(queues = {"hello.fanout"})
    public void receive2(Message message){
        System.out.println("receive2:"+new String(message.getBody(), CharsetUtil.UTF_8));
    }
}
