package com.haifeng.sbtx.api.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户角色关联表 前端控制器
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@RestController
@RequestMapping("/user-role")
public class UserRoleController {

}

