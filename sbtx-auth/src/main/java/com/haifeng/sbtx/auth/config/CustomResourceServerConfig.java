package com.haifeng.sbtx.auth.config;

import com.haifeng.sbtx.auth.exception.AuthExceptionEntryPoint;
import com.haifeng.sbtx.auth.exception.CustomAccessDeniedHandler;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;


/**
 * <p>
 *  资源服务器配置中心
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-15
 */
@Configuration
@AllArgsConstructor
@EnableResourceServer
public class CustomResourceServerConfig extends ResourceServerConfigurerAdapter {

    private final AuthExceptionEntryPoint authExceptionEntryPoint;
    private final CustomAccessDeniedHandler customAccessDeniedHandler;

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.formLogin()
            .and()
            .authorizeRequests()
                .antMatchers("/noAuth/**").permitAll()
                .anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.authenticationEntryPoint(authExceptionEntryPoint)
                .accessDeniedHandler(customAccessDeniedHandler);
    }

}
