package com.haifeng.sbtx.auth.config;

import com.haifeng.sbtx.auth.handler.MyRedisTokenStore;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * <p>
 *  Redis存储令牌
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-15
 */
@Configuration
@AllArgsConstructor
public class RedisTokenStoreConfig{

    private final RedisConnectionFactory redisConnectionFactory;

    /**
     * 设置令牌存储方式:Redis存储
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        return new MyRedisTokenStore(redisConnectionFactory);
    }
}
