package com.haifeng.sbtx.auth.service.impl;


import com.haifeng.sbtx.mapper.entity.dto.UserDto;
import com.haifeng.sbtx.mapper.mapper.UserMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * <p>
 *  自定义UserDetailsService
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-04
 */
@Slf4j
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //根据用户名查询出用户信息及权限信息
        UserDto userDto = userMapper.getUserByUsername(username);

        if (userDto == null) {
            throw new UsernameNotFoundException("用户不存在: " + username);
        }
        return userDto;
    }
}
