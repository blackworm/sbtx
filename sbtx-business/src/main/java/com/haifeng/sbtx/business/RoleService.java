package com.haifeng.sbtx.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.sbtx.mapper.entity.Role;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface RoleService extends IService<Role> {

}
