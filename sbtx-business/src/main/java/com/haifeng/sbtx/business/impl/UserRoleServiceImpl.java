package com.haifeng.sbtx.business.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.sbtx.business.UserRoleService;
import com.haifeng.sbtx.mapper.entity.UserRole;
import com.haifeng.sbtx.mapper.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
