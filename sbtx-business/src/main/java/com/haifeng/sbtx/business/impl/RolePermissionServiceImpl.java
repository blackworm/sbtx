package com.haifeng.sbtx.business.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.sbtx.business.RolePermissionService;
import com.haifeng.sbtx.mapper.entity.RolePermission;
import com.haifeng.sbtx.mapper.mapper.RolePermissionMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关联表 服务实现类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
