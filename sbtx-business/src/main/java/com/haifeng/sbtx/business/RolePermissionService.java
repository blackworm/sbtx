package com.haifeng.sbtx.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.sbtx.mapper.entity.RolePermission;

/**
 * <p>
 * 角色权限关联表 服务类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface RolePermissionService extends IService<RolePermission> {

}
