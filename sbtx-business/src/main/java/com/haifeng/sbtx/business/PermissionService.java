package com.haifeng.sbtx.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.sbtx.mapper.entity.Permission;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface PermissionService extends IService<Permission> {

}
